//
// Created by Phil Romig on 11/13/18.
//

#include "packetstats.h"
#define SIZE_ETHERNET 14

// ****************************************************************************
// * pk_processor()
// *  Most/all of the work done by the program will be done here (or at least it
// *  it will originate here). The function will be called once for every
// *  packet in the savefile.
// ****************************************************************************
void pk_processor(u_char *user, const struct pcap_pkthdr *pkthdr, const u_char *packet) {

    resultsC* results = (resultsC*)user;
    results->incrementTotalPacketCount();
    DEBUG << "Processing packet #" << results->packetCount() << ENDL;
    char s[256]; bzero(s,256); bcopy(ctime(&(pkthdr->ts.tv_sec)),s,strlen(ctime(&(pkthdr->ts.tv_sec)))-1);
    TRACE << "\tPacket timestamp is " << s;
    TRACE << "\tPacket capture length is " << pkthdr->caplen ;
    TRACE << "\tPacket physical length is " << pkthdr->len ;
    /*
    Link layer: Ethernet, IEE 802.3
    Network layer: IPv4, IPv6, Other-Network-Layer
    Transport Layer: TCP, UDP, ICMP, Other-Transport-Layer
    */
    // Link Layer
    // If ethernet -> ethernet ++
    // else IEEE ++
    struct ether_header *eptr;  /* net/ethernet.h */
 
    /* Headers Plus their offsets */
    eptr = (struct ether_header *) packet; // Ethernet
    struct ip *ipptr = (struct ip *) (packet + SIZE_ETHERNET); // IPv4
    struct ip6_hdr *ip6ptr = (struct ip6_hdr *) (packet + SIZE_ETHERNET); // IPv6

    /* check to see if we have an ip packet */
    bool ipv4packet = false;

    DEBUG << ntohs (eptr->ether_type);
    
    if (ntohs (eptr->ether_type) > ETH_P_802_3_MIN	){
        DEBUG << "(Ethernet II)";
        results->newEthernet(pkthdr->len);
        results->newSrcMac((*(uint64_t*)eptr->ether_shost << 16) >> 16);
        results->newDstMac((*(uint64_t*)eptr->ether_dhost << 16) >> 16);
    }
    else {
        DEBUG << "(IEEE 802.3)";
        results->newIEEE(pkthdr->len);

        results->newSrcMac((*(uint64_t*)eptr->ether_shost << 16) >> 16);
        results->newDstMac((*(uint64_t*)eptr->ether_dhost << 16) >> 16);
    }

    if (ntohs (eptr->ether_type) == ETH_P_ARP){
        results->newARP(pkthdr->len);
        DEBUG << "ARP";
        return;
    } else if (ntohs (eptr->ether_type) == ETH_P_IP){
        results->newIPv4(pkthdr->len);
        DEBUG << "IPv4";
        ipv4packet = true;
    } else if (ntohs (eptr->ether_type) == ETH_P_IPV6) {
        results->newIPv6(pkthdr->len);
        DEBUG << "IPv6";
        return;
    } else {
        DEBUG << "OTHER NETWORK";
        results->newOtherNetwork(pkthdr->len);
    }

    if (ipv4packet){
        results->newSrcIPv4(ipptr->ip_src.s_addr);
        results->newDstIPv4(ipptr->ip_dst.s_addr);

        // Get Header Length offset
        unsigned short int ipHeaderLen = ipptr->ip_hl * 4;

        // Check More Fragments flag
        if ((ntohs(ipptr->ip_off) & IP_MF) != 0){
            DEBUG << "FRAGMENTED PACKET";
            results->incrementFragCount();
        }

        // Get protocol
        DEBUG << "PROTOCOL " << (unsigned int) ipptr->ip_p;
        
        DEBUG << "HEADER LEN: " << ipptr->ip_hl;

        if (ipptr->ip_p == 17){ // is UDP
            TRACE << "PROTOCOL UDP";
            struct udphdr *udpptr = (struct udphdr *) (packet + SIZE_ETHERNET + ipHeaderLen);
            
            results->newUDP(pkthdr->len);
            results->newSrcUDP(htons(udpptr->source));
            results->newDstUDP(htons(udpptr->dest));
        } else if (ipptr->ip_p == 6){ // is TCP
            TRACE << "PROTOCOL TCP";

            struct tcphdr *tcpptr = (struct tcphdr *) (packet + SIZE_ETHERNET + ipHeaderLen);
            int TCPheaderlen = tcpptr->doff * 4;
            results->newTCP(pkthdr->len);
            
            results->newSrcTCP(htons(tcpptr->source));
            results->newDstTCP(htons(tcpptr->dest));

            if (tcpptr->syn) results->incrementSynCount();
            if (tcpptr->fin) results->incrementFinCount();

        } else if (ipptr->ip_p == 1){ // ICMP (Known to be off by 1)
            TRACE << "PROTOCOL ICMP";
            results->newICMP(pkthdr->len);

        } else { // is other
            TRACE << "PROTOCOL OTHER";
            results->newOtherTransport(pkthdr->len);
        }

    } /*else { // IPv6

        // TODO GET FRAGMENTS
        /*
        if ((ntohs(ipptr->ip_off) & IP_MF) != 0){
            DEBUG << "FRAGMENTED PACKET";
            results->incrementFragCount();
        }
        *
        // Get protocol
        TRACE << "PROTOCOL" << (unsigned int) ipptr->ip_p;

        if (ipptr->ip_p == 17){ // is UDP
            TRACE << "PROTOCOL UDP";
            results->newUDP(1);
        } else if (ipptr->ip_p == 6){ // is TCP
            TRACE << "PROTOCOL TCP";
            results->newTCP(1);
        } else if (ipptr->ip_p == 1){ // ICMP (Known to be off by 1)
            TRACE << "PROTOCOL ICMP";
            results->newICMP(1);
        } else if (ntohs (eptr->ether_type) != ETH_P_ARP) { // is other
            TRACE << "PROTOCOL OTHER";
            results->newOtherTransport(1);
        }

        //results->newSrcIPv6(ip6ptr->ip6_src.s_addr);
        //results->newDstIPv6(ip6ptr->ip6_dst.s_addr);

        //Figure out transport layer
    } */
    // Transport Layer

    // If IPv4
    /*
        Increment IPv4
        Figure out Transport layer
        if tcp
            The number of packets with the TCP-SYN bit set.
            The number of packets with the TCP-FIN bit set.
            The number of packets with the TCP-More-Fragments bit set.
    */

    // else IPv6++
        // Figure out Transport layer




    return;
}
